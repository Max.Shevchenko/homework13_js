
let size = 8;
let mines = Math.floor(size*size/6);
let flags = 0;





function makeBoard() {
    let table = document.createElement('TABLE');
    table.setAttribute('id', 'gameBoard');
    let tbody = document.createElement('TBODY');
    table.appendChild(tbody);
    for (let i = 0; i < size; i++) {
        let tr = document.createElement('TR');
        let col = document.createElement('COL');
        col.setAttribute('width', `${500/size}px`);
        tr.setAttribute('height', `${500/size}px`);
        table.appendChild(col);
        tbody.appendChild(tr);
        for (let j = 0; j < size; j++) {
            let td = document.createElement('TD');
            td.setAttribute('oncontextmenu', 'return false');
            td.style.backgroundColor = 'white';
            tr.appendChild(td);
        }
    }
    document.body.appendChild(table);
}

makeBoard(); //*

let gameArray = document.getElementById('gameBoard');





function minesRandom() {
    let minesSet = 0;
    while (minesSet < mines) {
        let k = Math.floor(Math.random()*(size));
        let l = Math.floor(Math.random()*(size));
        if (gameArray.rows[k].cells[l].innerText !== 'm') {
            gameArray.rows[k].cells[l].innerText = 'm';
            minesSet++;
        }
    }
}

minesRandom(); //*

function countMinesAround() {
    for (let k = 0; k < size; k++) {
        for (let l = 0; l < size; l++) {
            let minesAround = 0;
            if (gameArray.rows[k].cells[l].innerHTML !== 'm') {
                if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1].innerHTML === 'm') {
                    minesAround = minesAround + 1;
                }
                if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l + 1] !== undefined && gameArray.rows[k - 1].cells[l + 1].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k].cells[l + 1] !== undefined && gameArray.rows[k].cells[l + 1].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l - 1] !== undefined && gameArray.rows[k + 1].cells[l - 1].innerHTML === 'm') {
                    minesAround++;
                }
                if (gameArray.rows[k].cells[l - 1] !== undefined && gameArray.rows[k].cells[l - 1].innerHTML === 'm') {
                    minesAround++;
                }
                gameArray.rows[k].cells[l].innerText = minesAround;
            }
        }
    }
}

countMinesAround(); //*

function showOnClick() {
    let parent = document.getElementById('gameBoard');
    parent.addEventListener('click', show, false);
    parent.addEventListener('contextmenu', flag, false);
    parent.addEventListener('click', button, false);
    parent.addEventListener('dblclick', showIfFlagsAround);

    function button(elem) {
        if (elem.target !== elem.currentTarget) {
            let check = !!document.getElementById('start');
            if (check === false) {
                let button = document.createElement('button');
                button.setAttribute('id', 'start');
                button.setAttribute('onclick', 'tryAgain()');
                button.innerHTML = 'Начать игру заново';
                document.body.appendChild(button);
                document.getElementById('resize').remove();
            } else {
                parent.removeEventListener('click', button);
            }
        }
    }


    function show(elem) {
        if (elem.target !== elem.currentTarget) {
            elem.target.style.opacity = '1';
            elem.target.style.backgroundColor = 'grey';
            if (elem.target.style.backgroundColor === 'black') {
                elem.target.style.color = 'black';
                elem.target.style.backgroundColor = 'white';
                flags--;
                updateFlagCount();
            }
            showFieldsWithoutMines();
            if (elem.target.innerHTML === 'm') {
                elem.target.style.backgroundColor = 'red';
                setTimeout(mineBoom, 1000);
                parent.removeEventListener('click', show);
                parent.removeEventListener('contextmenu', flag);
            }
        }
        elem.stopPropagation();
    }

    function mineBoom() {
        for (let k = 0; k < size; k++) {
            for (let l = 0; l < size; l++) {
                gameArray.rows[k].cells[l].style.opacity = '1';
            }
        }
        document.body.innerHTML += `<span>Вы проиграли!</span>`;
    }

    function flag(elem) {
        if (elem.target !== elem.currentTarget) {
            if (elem.target.style.backgroundColor !== 'black' && elem.target.style.opacity !== '1' && elem.target.innerHTML === 'm') {
                elem.target.style.color = 'transparent';
                elem.target.style.opacity = '1';
                elem.target.style.backgroundColor = 'black';
                flags++;
                updateFlagCount();
            } else if (elem.target.style.backgroundColor !== 'grey' && elem.target.style.backgroundColor !== 'white') {
                elem.target.style.backgroundColor = 'white';
                elem.target.style.color = 'black';
                elem.target.style.opacity = '0';
                flags--;
                updateFlagCount();

            }
        }
        elem.stopPropagation();
    }
}

showOnClick(); //*

 function showFieldsWithoutMines() {
     for (let i = 0; i <= size*2; i++) {
         for (let k = 0; k < size; k++) {
             for (let l = 0; l < size; l++) {
                 if (gameArray.rows[k].cells[l].innerHTML === '0' && gameArray.rows[k].cells[l].style.opacity === '1' && gameArray.rows[k].cells[l].style.backgroundColor !== 'black') {
                     if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1] !== undefined && (gameArray.rows[k - 1].cells[l - 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k - 1].cells[l - 1].innerHTML)))) {
                         gameArray.rows[k - 1].cells[l - 1].style.opacity = '1';
                         gameArray.rows[k - 1].cells[l - 1].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k - 1] !== undefined && (gameArray.rows[k - 1].cells[l].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k - 1].cells[l].innerHTML)))) {
                         gameArray.rows[k - 1].cells[l].style.opacity = '1';
                         gameArray.rows[k - 1].cells[l].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l + 1] !== undefined && (gameArray.rows[k - 1].cells[l + 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k - 1].cells[l + 1].innerHTML)))) {
                         gameArray.rows[k - 1].cells[l + 1].style.opacity = '1';
                         gameArray.rows[k - 1].cells[l + 1].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k].cells[l + 1] !== undefined && (gameArray.rows[k].cells[l + 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k].cells[l + 1].innerHTML)))) {
                         gameArray.rows[k].cells[l + 1].style.opacity = '1';
                         gameArray.rows[k].cells[l + 1].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1] !== undefined && (gameArray.rows[k + 1].cells[l + 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k + 1].cells[l + 1].innerHTML)))) {
                         gameArray.rows[k + 1].cells[l + 1].style.opacity = '1';
                         gameArray.rows[k + 1].cells[l + 1].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k + 1] !== undefined && (gameArray.rows[k + 1].cells[l].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k + 1].cells[l].innerHTML)))) {
                         gameArray.rows[k + 1].cells[l].style.opacity = '1';
                         gameArray.rows[k + 1].cells[l].style.backgroundColor = 'grey';

                     }
                     if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l - 1] !== undefined && (gameArray.rows[k + 1].cells[l - 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k + 1].cells[l - 1].innerHTML)))) {
                         gameArray.rows[k + 1].cells[l - 1].style.opacity = '1';
                         gameArray.rows[k + 1].cells[l - 1].style.backgroundColor = 'grey';
                     }
                     if (gameArray.rows[k].cells[l - 1] !== undefined && (gameArray.rows[k].cells[l - 1].innerHTML === '0' || !Number.isNaN(Number(gameArray.rows[k].cells[l - 1].innerHTML)))) {
                         gameArray.rows[k].cells[l - 1].style.opacity = '1';
                         gameArray.rows[k].cells[l - 1].style.backgroundColor = 'grey';

                     }
                 }
             }
         }
     }
 }

 function showIfFlagsAround(elem) {
     if (elem.target !== elem.currentTarget) {
         for (let i = 0; i <= size*2; i++) {
             for (let k = 0; k < size; k++) {
                 for (let l = 0; l < size; l++) {
                     let flagCount = 0;
                     if (gameArray.rows[k].cells[l].innerHTML !== 'm' && gameArray.rows[k].cells[l].style.opacity === '1') {
                         if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l + 1] !== undefined && gameArray.rows[k - 1].cells[l + 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k].cells[l + 1] !== undefined && gameArray.rows[k].cells[l + 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l - 1] !== undefined && gameArray.rows[k + 1].cells[l - 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (gameArray.rows[k].cells[l - 1] !== undefined && gameArray.rows[k].cells[l - 1].style.backgroundColor === 'black') {
                             flagCount++;
                         }
                         if (flagCount === Number(gameArray.rows[k].cells[l].innerHTML)) {
                             if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1] !== undefined && gameArray.rows[k - 1].cells[l - 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k - 1].cells[l - 1].style.opacity = '1';
                                 gameArray.rows[k - 1].cells[l - 1].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l].style.backgroundColor !== 'black') {
                                 gameArray.rows[k - 1].cells[l].style.opacity = '1';
                                 gameArray.rows[k - 1].cells[l].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k - 1] !== undefined && gameArray.rows[k - 1].cells[l + 1] !== undefined && gameArray.rows[k - 1].cells[l + 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k - 1].cells[l + 1].style.opacity = '1';
                                 gameArray.rows[k - 1].cells[l + 1].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k].cells[l + 1] !== undefined && gameArray.rows[k].cells[l + 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k].cells[l + 1].style.opacity = '1';
                                 gameArray.rows[k].cells[l + 1].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1] !== undefined && gameArray.rows[k + 1].cells[l + 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k + 1].cells[l + 1].style.opacity = '1';
                                 gameArray.rows[k + 1].cells[l + 1].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l].style.backgroundColor !== 'black') {
                                 gameArray.rows[k + 1].cells[l].style.opacity = '1';
                                 gameArray.rows[k + 1].cells[l].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k + 1] !== undefined && gameArray.rows[k + 1].cells[l - 1] !== undefined && gameArray.rows[k + 1].cells[l - 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k + 1].cells[l - 1].style.opacity = '1';
                                 gameArray.rows[k + 1].cells[l - 1].style.backgroundColor = 'grey';
                             }
                             if (gameArray.rows[k].cells[l - 1] !== undefined && gameArray.rows[k].cells[l - 1].style.backgroundColor !== 'black') {
                                 gameArray.rows[k].cells[l - 1].style.opacity = '1';
                                 gameArray.rows[k].cells[l - 1].style.backgroundColor = 'grey';
                             }
                         }
                     }
                 }
             }
         }
     }
     elem.stopPropagation();
 }

function showFlagCount() {
    let div = document.createElement('div');
    div.setAttribute('id', 'flagCount');
    div.innerHTML = `${flags}/${mines}`;
    document.body.appendChild(div);
}

showFlagCount();

function updateFlagCount() {
    document.getElementById('flagCount').innerHTML = `${flags}/${mines}`;
    if (flags === mines) {
        for (let k = 0; k < size; k++) {
            for (let l = 0; l < size; l++) {
                gameArray.rows[k].cells[l].style.opacity = '1';
                gameArray.rows[k].cells[l].style.backgroundColor = 'grey';
                gameArray.rows[k].cells[l].style.color = 'black';
            }
        }
        let span = document.createElement('span');
        span.innerHTML = "Победа!";
        document.body.appendChild(span);
    }
}

function resizeButton() {
    let button = document.createElement('button');
    button.innerHTML = `Размер поля: ${size}`;
    button.setAttribute('id', 'resize');
    button.setAttribute('onclick', 'resizeFunction()');
    document.body.appendChild(button);
}

resizeButton();

function restart() {
    mines = Math.floor(size*size/6);
    document.body.innerHTML = '';
    flags = 0;
    makeBoard();
    gameArray = document.getElementById('gameBoard');
    minesRandom();
    countMinesAround();
    showOnClick();
    showFlagCount();
    resizeButton();
}

function resizeFunction() {
    size = Number(prompt("Введите размер поля"));
    restart();
}


 function tryAgain() {
     size = 8;
     restart();
 }


















































